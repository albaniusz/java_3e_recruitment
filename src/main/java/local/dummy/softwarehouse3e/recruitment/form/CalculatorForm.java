package local.dummy.softwarehouse3e.recruitment.form;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class CalculatorForm {
    @NotEmpty
    private double val1;

    @NotEmpty
    private double val2;
}
