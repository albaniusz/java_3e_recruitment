package local.dummy.softwarehouse3e.recruitment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResultDTO {
    private double value;
}
