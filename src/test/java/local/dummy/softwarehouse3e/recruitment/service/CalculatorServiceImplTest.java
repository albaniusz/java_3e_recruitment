package local.dummy.softwarehouse3e.recruitment.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CalculatorServiceImplTest {
    private final CalculatorService calculatorService = new CalculatorServiceImpl();

    @Test
    public void add_shouldAddTwoNumbers() {
        // given
        double val1 = 1.0;
        double val2 = 2.0;

        // when
        double result = calculatorService.addValues(val1, val2);

        // then
        assertEquals(3.0, result);
    }

    @Test
    public void add_shouldAddNumberAndZero() {
        // given
        double val1 = 1.0;
        double val2 = 0;

        // when
        double result = calculatorService.addValues(val1, val2);

        // then
        assertEquals(1.0, result);
    }

    @Test
    public void add_shouldAddTwoNegativeNumbers() {
        // given
        double val1 = -1.0;
        double val2 = -2.0;

        // when
        double result = calculatorService.addValues(val1, val2);

        // then
        assertEquals(-3.0, result);
    }


    @Test
    public void div_shouldDivideNumber() throws Exception {
        // given
        double val1 = 1.0;
        double val2 = 2.0;

        // when
        double result = calculatorService.divideValue(val1, val2);

        // then
        assertEquals(0.5, result);
    }

    @Test
    public void div_shouldDivideNumberByNegative() throws Exception {
        // given
        double val1 = 1.0;
        double val2 = -2.0;

        // when
        double result = calculatorService.divideValue(val1, val2);

        // then
        assertEquals(-0.5, result);
    }

    @Test
    public void div_shouldNotDivideNumberByZero() throws Exception {
        // given
        double val1 = 1.0;
        double val2 = 0;

        // when // then
        assertThrows(ArithmeticException.class, () -> calculatorService.divideValue(val1, val2));
    }
}
