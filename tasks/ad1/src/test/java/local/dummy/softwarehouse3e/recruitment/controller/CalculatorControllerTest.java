package local.dummy.softwarehouse3e.recruitment.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CalculatorControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void add_shouldAddTwoNumbersWithoutFraction() throws Exception {
        // given // when // then
        mockMvc.perform(configurePOSTBuilder("/add", "1.0", "2.0"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"value\":3.0}")));
    }

    @Test
    public void add_shouldAddTwoNumbersWithZeroValue() throws Exception {
        // given // when // then
        mockMvc.perform(configurePOSTBuilder("/add", "0.0", "0.0"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"value\":0.0}")));
    }

    @Test
    public void add_shouldAddTwoNumbersWithNegativeValue() throws Exception {
        // given // when // then
        mockMvc.perform(configurePOSTBuilder("/add", "1.0", "-2.0"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"value\":-1.0}")));
    }

    @Test
    public void add_shouldAddTwoNumbersWithFraction() throws Exception {
        // given // when // then
        mockMvc.perform(configurePOSTBuilder("/add", "1.2", "2.3"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"value\":3.5}")));
    }

    @Test
    public void add_shouldFoundWrongMethod() throws Exception {
        // given // when // then
        mockMvc.perform(configureGETBuilder("/add", "1.5", "2.5"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void add_shouldFoundBadParamsType() throws Exception {
        // given // when // then
        mockMvc.perform(configurePOSTBuilder("/add", "1.0", "abc"))
                .andExpect(status().is(400));
    }

    @Test
    public void div_shouldDivideNumberWithoutFraction() throws Exception {
        // given // when // then
        mockMvc.perform(configureGETBuilder("/div", "1.0", "2.0"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"value\":0.5}")));
    }

    @Test
    public void div_shouldDivideNumberWithFraction() throws Exception {
        // given // when // then
        mockMvc.perform(configureGETBuilder("/div", "1.5", "2.5"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"value\":0.6}")));
    }

    @Test
    public void div_shouldDivideNumberByNegative() throws Exception {
        // given // when // then
        mockMvc.perform(configureGETBuilder("/div", "1.5", "-2.5"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("{\"value\":-0.6}")));
    }

    @Test
    public void div_shouldNotDivideNumberByZero() throws Exception {
        // given // when // then
        mockMvc.perform(configureGETBuilder("/div", "1.0", "0.0"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void div_shouldFoundWrongMethod() throws Exception {
        // given // when // then
        mockMvc.perform(configurePOSTBuilder("/div", "1.5", "2.5"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void div_shouldFoundBadParamsType() throws Exception {
        // given // when // then
        mockMvc.perform(configureGETBuilder("/div", "1.0", "abc"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void div_shouldFoundNotEnoughParams() throws Exception {
        // given
        MockHttpServletRequestBuilder builder = get("/div")
                .param("val1", "1.0");

        // when // then
        mockMvc.perform(builder).andExpect(status().is4xxClientError());
    }

    private MockHttpServletRequestBuilder configureGETBuilder(String urlTemplate, String value1, String value2) {
        return get(urlTemplate)
                .param("val1", value1)
                .param("val2", value2);
    }

    private MockHttpServletRequestBuilder configurePOSTBuilder(String urlTemplate, String value1, String value2) {
        return post(urlTemplate)
                .param("val1", value1)
                .param("val2", value2);
    }
}
