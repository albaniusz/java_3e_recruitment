package local.dummy.softwarehouse3e.recruitment.service;

import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class CalculatorServiceImpl implements CalculatorService {
    @Override
    @Transactional
    public double addValues(double val1, double val2) {
        return val1 + val2;
    }

    @Override
    @Transactional
    public double divideValue(double val1, double val2) throws ArithmeticException {
        if (val2 == 0.0) {
            throw new ArithmeticException("Can not divide by zero");
        }

        return val1 / val2;
    }
}
