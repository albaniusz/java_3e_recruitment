package local.dummy.softwarehouse3e.recruitment.controller;

import local.dummy.softwarehouse3e.recruitment.dto.ResultDTO;
import local.dummy.softwarehouse3e.recruitment.form.CalculatorForm;
import local.dummy.softwarehouse3e.recruitment.form.CalculatorFormException;
import local.dummy.softwarehouse3e.recruitment.service.CalculatorService;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class CalculatorController {
    private final CalculatorService calculatorService;

    public CalculatorController(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    @PostMapping("/add")
    public ResultDTO add(@ModelAttribute @Valid CalculatorForm form, BindingResult result) throws Exception {
        if (result.hasErrors()) {
            throw new CalculatorFormException("Input data is not valid");
        }

        return new ResultDTO(calculatorService.addValues(form.getVal1(), form.getVal2()));
    }

    @GetMapping("/div")
    public ResultDTO div(@RequestParam double val1, @RequestParam double val2) {
        return new ResultDTO(calculatorService.divideValue(val1, val2));
    }
}
