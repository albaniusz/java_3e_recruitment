package local.dummy.softwarehouse3e.recruitment.service;

public interface CalculatorService {
    double addValues(double val1, double val2);

    double divideValue(double val1, double val2) throws ArithmeticException;
}
