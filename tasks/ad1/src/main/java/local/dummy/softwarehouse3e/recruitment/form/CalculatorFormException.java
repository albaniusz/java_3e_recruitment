package local.dummy.softwarehouse3e.recruitment.form;

public class CalculatorFormException extends Exception {
    public CalculatorFormException(String message) {
        super(message);
    }
}
