DO $$
DECLARE r record;
DECLARE lastElement integer;
DECLARE beforeLastElement integer;
BEGIN
	CREATE TEMP TABLE IF NOT EXISTS test (liczby numeric) with (oids = false);
	DELETE FROM test;

	FOR r IN SELECT liczba FROM public.liczby ORDER BY id ASC
    LOOP
		RAISE NOTICE 'liczba: %', r.liczba;
		RAISE NOTICE 'poprzednia: %', lastElement;
		RAISE NOTICE 'przed poprzednia: %', beforeLastElement;
		
		IF
			lastElement = r.liczba AND beforeLastElement = r.liczba
		THEN
			RAISE NOTICE 'zliczam liczbe %', r.liczba;
			INSERT INTO test (liczby) VALUES (r.liczba);
		END IF;
		
		beforeLastElement := lastElement;
		lastElement := r.liczba;
		
    END LOOP;
END$$;

SELECT DISTINCT liczby FROM test ORDER BY liczby ASC;
