#!/bin/bash
echo "Start client"

sleep 1

printf "\n\nEndpoint /add\n\n"

curl -v http://localhost:8080/add -d "val1=1.2&val2=2.2" | jq

sleep 3

printf "\n\nEndpoint /div\n\n"

curl -v "http://localhost:8080/div?val1=1.1&val2=2.2" | jq

sleep 1

printf "\n\nClient done!"
